package cat.itb.eduardpetrache7e4.dam.m03.uf4.exercices.translator

interface Translator {
    fun translate(text: String): String
}