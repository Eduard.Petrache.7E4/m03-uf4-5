package cat.itb.eduardpetrache7e4.dam.m03.uf4.exercices.carpentryshop

data class Llisto(val pricePerMeter: Double, val length: Double) : Product {
    override fun calculatePrice(): Double = pricePerMeter * length
}