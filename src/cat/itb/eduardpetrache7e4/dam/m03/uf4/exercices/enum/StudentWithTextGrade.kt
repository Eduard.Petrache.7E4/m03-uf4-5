package cat.itb.eduardpetrache7e4.dam.m03.uf4.exercices.enum

enum class Grade {
    SUSPES,
    APROVAT,
    BE,
    NOTABLE,
    EXCELLENT
}

class Student(private val name: String, private val textGrade: Grade) {
    override fun toString(): String {
        return "Student{name='$name', textGrade=$textGrade}"
    }
}

fun main() {
    val student1 = Student("Mar", Grade.SUSPES)
    val student2 = Student("Joan", Grade.EXCELLENT)
    println(student1)
    println(student2)
}
