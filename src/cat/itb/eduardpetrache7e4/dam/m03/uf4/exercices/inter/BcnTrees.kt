package cat.itb.eduardpetrache7e4.dam.m03.uf4.exercices.inter


fun main() {
    val finder: TreeFinder = BcnTreeFinder()

    // Llegim el nom científic de l'usuari
    print("Introdueixi el nom científic de l'arbre: ")
    val scientificName = readLine() ?: ""

    // Calculem el nombre d'arbres amb el nom científic especificat
    val count = finder.countTreesByScientificName(scientificName)

    // Mostrem el resultat
    println("Hi ha $count arbres de l'espècie $scientificName a la ciutat de Barcelona.")
}
