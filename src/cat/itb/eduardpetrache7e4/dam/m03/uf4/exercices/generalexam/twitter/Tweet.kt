package cat.itb.eduardpetrache7e4.dam.m03.uf4.exercices.generalexam.twitter

open class TweetModel(private val usuari: String, private val data: String, private val text: String) {
}

class Tweet (private val usuari: String, private val data: String, private val text: String): TweetModel(usuari, data, text) {
    override fun toString(): String {
        return "@$usuari · $data\n$text\n\n"
    }
}

class PollTweet(private val usuari: String, private val data: String, private val text: String, val entrees: String ) : TweetModel(usuari, data, text) {
    override fun toString(): String {
        return "@$usuari · $data\n$text\n\n$entrees"
    }

//    private val itemList = mutableListOf<String>()

//    fun createpoll(entrees: String){
//        itemList.add(entrees)
//    }
}