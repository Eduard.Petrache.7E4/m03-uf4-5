package cat.itb.eduardpetrache7e4.dam.m03.uf4.exercices.quiz

class MultipleChoiceQuestion(text: String, val options: List<String>, val correctAnswer: String) : Question(text) {
    override fun checkAnswer(answer: String): Boolean {
        return answer.equals(correctAnswer, ignoreCase = true)
    }
}