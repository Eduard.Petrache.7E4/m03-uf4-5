package cat.itb.eduardpetrache7e4.dam.m03.uf4.exercices.instrumentsimulator

class Triangle(private val resonance: Int) : Instrument() {
    override fun makeSound(): String {
        return "T" + "I".repeat(resonance) + "NC"
    }
}