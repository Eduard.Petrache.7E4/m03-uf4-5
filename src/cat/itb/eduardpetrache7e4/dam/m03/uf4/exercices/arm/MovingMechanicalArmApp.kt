package cat.itb.eduardpetrache7e4.dam.m03.uf4.exercices.arm

class MovingMechanicalArmApp(altitude: Double = 0.0, var position: Double = 0.0, turnedOn: Boolean = false) : MechanicalArmApp(0.0, altitude, turnedOn) {

    fun move(distance: Double) {
        if (turnedOn && position + distance >= 0) {
            position += distance
            println(this)
        }
    }

    override fun toString(): String {
        return "MovingMechanicalArm{openAngle=$openAngle, altitude=$altitude, position=$position, turnedOn=$turnedOn}"
    }
}

fun main() {
    val arm = MovingMechanicalArmApp()
    arm.toggle()
    arm.updateAltitude(3.0)
    arm.move(4.5)
    arm.updateAngle(180.0)
    arm.updateAltitude(-3.0)
    arm.updateAngle(-180.0)
    arm.updateAltitude(3.0)
    arm.move(-4.5)
    arm.toggle()
}