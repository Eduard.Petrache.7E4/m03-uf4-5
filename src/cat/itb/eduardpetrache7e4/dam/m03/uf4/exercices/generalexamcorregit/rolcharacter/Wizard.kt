package cat.itb.eduardpetrache7e4.dam.m03.uf4.exercices.generalexamcorregit.rolcharacter

class Wizard(name: String, healthPoints: Int) : Character(name, healthPoints) {
    override fun attack(opponent: IRolCharacter) {
        super.attack(opponent)
        println("$name is casting a spell on ${opponent.name}")
    }
}