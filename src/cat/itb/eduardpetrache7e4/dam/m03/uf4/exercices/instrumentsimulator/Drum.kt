package cat.itb.eduardpetrache7e4.dam.m03.uf4.exercices.instrumentsimulator

class Drum(private val tone: String) : Instrument() {
    override fun makeSound(): String {
        return when (tone) {
            "A" -> "TAAAM"
            "O" -> "TOOOM"
            "U" -> "TUUUM"
            else -> "Unknown tone"
        }
    }
}