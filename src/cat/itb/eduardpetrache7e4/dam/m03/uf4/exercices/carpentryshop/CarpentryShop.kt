package cat.itb.eduardpetrache7e4.dam.m03.uf4.exercices.carpentryshop

class CarpentryShop(val products: List<Product>) {
    fun getTotalPrice(): Double = products.sumOf { it.calculatePrice() }
}

fun main() {
    print("Enter the number of products: ")
    val numProducts = readLine()?.toIntOrNull() ?: return

    val products = mutableListOf<Product>()

    repeat(numProducts) {
        print("Enter product info (e.g. Taulell 15 20 12 or Llistó 12 4): ")
        val productInfo = readLine()?.split(" ") ?: return@repeat

        when (productInfo[0].toLowerCase()) {
            "t" -> {
                val pricePerSquareMeter = productInfo[1].toDoubleOrNull() ?: return@repeat
                val length = productInfo[2].toDoubleOrNull() ?: return@repeat
                val width = productInfo[3].toDoubleOrNull() ?: return@repeat
                val board = Taulell(pricePerSquareMeter, length, width)
                products.add(board)
            }
            "l" -> {
                val pricePerMeter = productInfo[1].toDoubleOrNull() ?: return@repeat
                val length = productInfo[2].toDoubleOrNull() ?: return@repeat
                val strip = Llisto(pricePerMeter, length)
                products.add(strip)
            }
            else -> return@repeat
        }
    }

    val shop = CarpentryShop(products)
    println("El preu total és: ${shop.getTotalPrice()}€")
}
