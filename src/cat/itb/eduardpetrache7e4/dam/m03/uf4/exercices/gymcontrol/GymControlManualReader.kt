package cat.itb.eduardpetrache7e4.dam.m03.uf4.exercices.gymcontrol

import java.util.*

class GymControlManualReader(val scanner: Scanner = Scanner(System.`in`)) : GymControlReader {
    override fun nextId() = scanner.next()
}