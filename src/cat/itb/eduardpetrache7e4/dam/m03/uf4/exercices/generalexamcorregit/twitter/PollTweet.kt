package cat.itb.eduardpetrache7e4.dam.m03.uf4.exercices.generalexamcorregit.twitter

class PollTweet(user: String, date: String, text: String, val options: List<String>) : Tweet(user, date, text) {
    private val votes = IntArray(options.size)

    fun vote(index: Int) {
        if (index in options.indices) {
            votes[index]++
        }
    }

    override fun print() {
        super.print()
        for (i in options.indices) {
            println("- (${votes[i]} vots) ${options[i]}")
        }
        println()
    }
}