package cat.itb.eduardpetrache7e4.dam.m03.uf4.exercices.gymcontrol

interface GymControlReader {
    fun nextId() : String
}