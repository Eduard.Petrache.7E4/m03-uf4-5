package cat.itb.eduardpetrache7e4.dam.m03.uf4.exercices.carpentryshop

data class Taulell(val pricePerSquareMeter: Double, val length: Double, val width: Double) : Product {
    override fun calculatePrice(): Double = pricePerSquareMeter * length * width
}