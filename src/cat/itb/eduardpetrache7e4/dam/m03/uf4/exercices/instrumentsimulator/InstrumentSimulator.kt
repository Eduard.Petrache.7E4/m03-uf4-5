package cat.itb.eduardpetrache7e4.dam.m03.uf4.exercices.instrumentsimulator

fun main() {
    val instruments: List<Instrument> = listOf(
        Triangle(5),
        Drum("A"),
        Drum("O"),
        Triangle(1),
        Triangle(5)
    )
    play(instruments)
}

private fun play(instruments: List<Instrument>) {
    for (instrument in instruments) {
        instrument.makeSounds(2) // plays 2 times the sound
    }
}