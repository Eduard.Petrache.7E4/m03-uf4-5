package cat.itb.eduardpetrache7e4.dam.m03.uf4.exercices.generalexam.twitter.rolcharacter

class Warrior (override var name: String, override var healthPoints: Int): Character("Warrior", name, healthPoints){
    override fun attack(opponent: Character) {
        println("$name is using a sword to attack Legolas")
    }

    override fun takeDamage(damage: Int) {
        TODO("Not yet implemented")
    }
}
