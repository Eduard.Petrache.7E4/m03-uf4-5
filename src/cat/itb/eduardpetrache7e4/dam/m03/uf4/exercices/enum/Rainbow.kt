package cat.itb.eduardpetrache7e4.dam.m03.uf4.exercices.enum

enum class RainbowColors {
    RED, ORANGE, YELLOW, GREEN, BLUE, INDIGO, VIOLET
}

fun isInColorList(color: String) : Boolean {
    for (enum in RainbowColors.values()) {
        if (color.uppercase() == enum.name) return true
    }
    return false
}

fun main() {
    val color = readln()

    val pertanyArc = isInColorList(color)

    println(pertanyArc)
}








