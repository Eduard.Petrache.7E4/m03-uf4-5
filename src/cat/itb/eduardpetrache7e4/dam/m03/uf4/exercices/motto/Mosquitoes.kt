package cat.itb.eduardpetrache7e4.dam.m03.uf4.exercices.motto

class Mosquitoes : Team {
    override val name = "Mosquitoes"
    override val motto = "Bzzzanyarem"

    override fun shoutMotto() {
        println(motto)
    }
}
