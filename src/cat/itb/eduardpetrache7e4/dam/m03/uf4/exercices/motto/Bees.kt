package cat.itb.eduardpetrache7e4.dam.m03.uf4.exercices.motto

class Bees : Team {
    override val name = "Bees"
    override val motto = "Piquem Fort"

    override fun shoutMotto() {
        println(motto)
    }
}
