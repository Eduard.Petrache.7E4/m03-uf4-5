package cat.itb.eduardpetrache7e4.dam.m03.uf4.exercices.generalexamcorregit.twitter

open class Tweet(val user: String, val date: String, val text: String) {
    open fun print() {
        println("@$user · $date\n$text\n")
    }
}