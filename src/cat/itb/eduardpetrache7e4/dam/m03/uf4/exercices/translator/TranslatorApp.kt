package cat.itb.eduardpetrache7e4.dam.m03.uf4.exercices.translator


class SameLanguageTranslator : Translator {
    override fun translate(text: String): String {
        return text
    }
}

class TranslatorApp(private val translator: Translator) {
    fun start() {
        var input: String?
        var output = ""

        while (true) {
            print("> ")
            input = readLine()
            if (input == null || input.isBlank()) break
            output += translator.translate(input) + "\n"
            if (input.endsWith(".")) break
        }

        println(output.trim())
    }
}


fun main() {
    val translator = SameLanguageTranslator()
    val app = TranslatorApp(translator)
    app.start()
}
