package cat.itb.eduardpetrache7e4.dam.m03.uf4.exercices.inter

data class Tree(val espai_verd: String, val adreca: String, val nom_cientific: String, val familia: String, val orige: String)
