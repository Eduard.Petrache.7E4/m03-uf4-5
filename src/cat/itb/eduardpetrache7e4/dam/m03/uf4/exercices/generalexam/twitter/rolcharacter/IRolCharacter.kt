package cat.itb.eduardpetrache7e4.dam.m03.uf4.exercices.generalexam.twitter.rolcharacter

interface IRolCharacter {
    var name: String
    var healthPoints: Int
    fun attack(opponent: Character)
    fun takeDamage(damage: Int)
}