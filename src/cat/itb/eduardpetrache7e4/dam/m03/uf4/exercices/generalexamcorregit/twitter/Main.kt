package cat.itb.eduardpetrache7e4.dam.m03.uf4.exercices.generalexamcorregit.twitter

fun main() {
    val tweet1 = Tweet("iamdevloper", "07 de gener", "Remember, a few hours of trial and error can save you several minutes of looking at the README")
    val tweet2 = Tweet("softcatala", "29 de març", "Avui mateix, #CommonVoiceCAT segueix creixent 🚀:\n🗣️ 856 hores enregistrades\n✅ 725 de validades.\nSi encara no has participat, pots fer-ho aquí!")
    val pollTweet3 = PollTweet("musicat", "02 d'abril", "Quina cançó t'agrada més?", listOf(
        "Comèdia dramàtica - La Fúmiga",
        "In the night - Oques Grasses",
        "Una Lluna a l'Aigua - Txarango",
        "Esbarzers - La Gossa Sorda"
    ))
    val tweet4 = Tweet("ProgrammerJokes", "05 d'abril", "Q: what's the object-oriented way to become wealthy?\n\nA: Inheritance")

    pollTweet3.vote(0)
    pollTweet3.vote(0)
    pollTweet3.vote(0)
    pollTweet3.vote(1)
    pollTweet3.vote(1)
    pollTweet3.vote(2)
    pollTweet3.vote(2)
    pollTweet3.vote(3)
    pollTweet3.vote(3)
    pollTweet3.vote(3)
    pollTweet3.vote(3)

    tweet1.print()
    tweet2.print()
    tweet4.print()
    pollTweet3.print()
}