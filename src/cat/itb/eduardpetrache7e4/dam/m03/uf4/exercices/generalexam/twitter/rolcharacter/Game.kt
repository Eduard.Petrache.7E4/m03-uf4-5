package cat.itb.eduardpetrache7e4.dam.m03.uf4.exercices.generalexam.twitter.rolcharacter

class Game {
    private var characters = mutableListOf<IRolCharacter>()

    /**
     * Create diferent character depending on characterType and is *added to the list characters
     */
    fun createCharacter(characterType: String, name: String, healthPoints: Int) {
        when (characterType) {
            "Warrior" -> characters.add(Warrior(name, healthPoints))
            "Wizard" -> characters.add(Wizard(name, healthPoints))
            "Archer" -> characters.add(Archer(name, healthPoints))
            else -> println("Invalid character type.")
        }
    }

    /**
     * Each turn start the battle between two characters until only one * remains standing.
     */
    fun startBattle() {
        while (characters.size > 1) {
            val attacker = getCharacter()
            var opponent = getCharacter()
            while (opponent== attacker) {
                opponent = getCharacter()
            }

//            attacker.attack(opponent)
            println("-------")
            if (opponent.healthPoints <= 0) {
                characters.remove(opponent)
            }
        }
        println("${characters[0].name} is the winner!")
    }

    /**
     * Selects a character from the list of characters
     */
    private fun getCharacter(): IRolCharacter {
        val attackerIndex = (0 until characters.size).random()
        return characters[attackerIndex]
    }
}

fun main() {
    val game = Game()
    game.createCharacter("Warrior", "Conan", 50)
    game.createCharacter("Wizard", "Gandalf", 40)
    game.createCharacter("Archer", "Legolas", 30)
    game.createCharacter("Warrior", "Thor", 60)
    game.startBattle()
}