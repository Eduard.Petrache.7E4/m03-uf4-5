package cat.itb.eduardpetrache7e4.dam.m03.uf4.exercices.quiz

import java.util.*


class Quiz(val questions: List<Question>) {
    fun runQuiz() {
        var correctAnswers = 0
        val scanner = Scanner(System.`in`)
        for (question in questions) {
            println(question.text)
            when (question) {
                is FreeTextQuestion -> {
                    val userAnswer = scanner.nextLine()
                    if (question.checkAnswer(userAnswer)) {
                        correctAnswers++
                    }
                }
                is MultipleChoiceQuestion -> {
                    for ((index, option) in question.options.withIndex()) {
                        println("${index + 1}. $option")
                    }
                    val userAnswerIndex = scanner.nextInt() - 1
                    scanner.nextLine()
                    val userAnswer = question.options[userAnswerIndex]
                    if (question.checkAnswer(userAnswer)) {
                        correctAnswers++
                    }
                }
            }
        }
        println("Has respost correctament a $correctAnswers de ${questions.size} preguntes.")
    }
}

fun main() {
    val quiz = Quiz(listOf(
        FreeTextQuestion("Quin és el nom de la capital de Rumanía?", "Bucarest"),
        MultipleChoiceQuestion("Quina de les següents és una fruita?", listOf("Pastanaga", "Poma", "Brocoli"), "Poma"),
        FreeTextQuestion("Quant fan 2 + 2?", "4")
    ))

    quiz.runQuiz()
}
