package cat.itb.eduardpetrache7e4.dam.m03.uf4.exercices.figures

class Figure(private var color: String) {
    fun prepareColor() {
        print(color)
    }

    fun clearColor() {
        print(RESET)
    }

    companion object {
        const val RESET = "\u001b[0m" // Text Reset
        const val BLACK = "\u001b[0;30m" // BLACK
        const val RED = "\u001b[0;31m" // RED
        const val GREEN = "\u001b[0;32m" // GREEN
        const val YELLOW = "\u001b[0;33m" // YELLOW
        const val BLUE = "\u001b[0;34m" // BLUE
        const val PURPLE = "\u001b[0;35m" // PURPLE
        const val CYAN = "\u001b[0;36m" // CYAN
        const val WHITE = "\u001b[0;37m" // WHITE
    }
}

class LeftPiramidFigures(private val base: Int, private val figure: Figure) {
    fun paint() {
        figure.prepareColor()
        for (i in 1..base) {
            for (j in 1..i) {
                print("X")
            }
            println()
        }
        figure.clearColor()
    }
}

class RectangleFigures(private val width: Int, private val height: Int, private val figure: Figure) {
    fun paint() {
        figure.prepareColor()
        for (i in 1..height) {
            for (j in 1..width) {
                print("X")
            }
            println()
        }
        figure.clearColor()
    }
}


fun main() {
    val red = Figure(Figure.RED)
    val yellow = Figure(Figure.YELLOW)
    val green = Figure(Figure.GREEN)

    val rectangle1 = RectangleFigures(5, 4, red)
    val triangle1 = LeftPiramidFigures(3, yellow)
    val rectangle3 = RectangleFigures(5, 3, green)

    rectangle1.paint()
    println()
    triangle1.paint()
    println()
    rectangle3.paint()
}

