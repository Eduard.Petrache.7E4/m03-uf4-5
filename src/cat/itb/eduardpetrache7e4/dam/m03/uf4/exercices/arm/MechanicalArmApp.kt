package cat.itb.eduardpetrache7e4.dam.m03.uf4.exercices.arm

open class MechanicalArmApp(var openAngle: Double = 0.0, var altitude: Double = 0.0, var turnedOn: Boolean = false) {

    fun toggle() {
        turnedOn = !turnedOn
        println(this)
    }

    fun updateAngle(angle: Double) {
        if (turnedOn) {
            openAngle = (openAngle + angle) % 360
            println(this)
        }
    }

    fun updateAltitude(delta: Double) {
        if (turnedOn && altitude + delta >= 0) {
            altitude += delta
            println(this)
        }
    }

    override fun toString(): String {
        return "MechanicalArm{openAngle=$openAngle, altitude=$altitude, turnedOn=$turnedOn}"
    }
}

fun main() {
    val arm = MechanicalArmApp()
    arm.toggle()
    arm.updateAltitude(3.0)
    arm.updateAngle(180.0)
    arm.updateAltitude(-3.0)
    arm.updateAngle(-180.0)
    arm.updateAltitude(3.0)
    arm.toggle()
}
