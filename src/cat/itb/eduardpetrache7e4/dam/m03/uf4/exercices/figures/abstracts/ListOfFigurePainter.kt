package cat.itb.eduardpetrache7e4.dam.m03.uf4.exercices.figures.abstracts

abstract class Figure(private var color: String) {
    abstract fun paint()

    fun paintText(text: String) {
        prepareColor()
        print(text)
        clearColor()
    }

    fun prepareColor() {
        print(color)
    }

    fun clearColor() {
        print(RESET)
    }

    companion object {
        const val RESET = "\u001b[0m" // Text Reset
        const val BLACK = "\u001b[0;30m" // BLACK
        const val RED = "\u001b[0;31m" // RED
        const val GREEN = "\u001b[0;32m" // GREEN
        const val YELLOW = "\u001b[0;33m" // YELLOW
        const val BLUE = "\u001b[0;34m" // BLUE
        const val PURPLE = "\u001b[0;35m" // PURPLE
        const val CYAN = "\u001b[0;36m" // CYAN
        const val WHITE = "\u001b[0;37m" // WHITE
    }
}

class LeftPiramidFigure3(private val base: Int, color: String) : Figure(color) {
    override fun paint() {
        prepareColor()
        for (i in 1..base) {
            for (j in 1..i) {
                print("X")
            }
            println()
        }
        clearColor()
    }
}

class RectangleFigure3(private val width: Int, private val height: Int, color: String) : Figure(color) {
    override fun paint() {
        prepareColor()
        for (i in 1..height) {
            for (j in 1..width) {
                print("X")
            }
            println()
        }
        clearColor()
    }
}

class PiramidFigure(private val height: Int, color: String) : Figure(color) {
    override fun paint() {
        prepareColor()
        for (i in 1..height) {
            for (j in 1..height - i) {
                print(" ")
            }
            for (j in 1..2 * i - 1) {
                print("X")
            }
            println()
        }
        clearColor()
    }
}


class ListOfFigurePainter(private val figures: List<Figure>) {
    fun paintAll() {
        for (figure in figures) {
            figure.paint()
            println()
        }
    }
}

fun main() {
    val figures = listOf(
        RectangleFigure3(5, 4, Figure.RED),
        LeftPiramidFigure3(3, Figure.YELLOW),
        RectangleFigure3(5, 3, Figure.GREEN),
        PiramidFigure(3, Figure.BLUE)
    )

    val painter = ListOfFigurePainter(figures)
    painter.paintAll()
}
