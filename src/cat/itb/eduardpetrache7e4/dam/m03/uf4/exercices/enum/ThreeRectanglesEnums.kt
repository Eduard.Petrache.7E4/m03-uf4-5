package cat.itb.eduardpetrache7e4.dam.m03.uf4.exercices.enum

import cat.itb.eduardpetrache7e4.dam.m03.uf4.exercices.figures.*

enum class Figure(val value: String) {
    RESET("\u001b[0m"),
    BLACK("\u001b[0;30m"),
    RED("\u001b[0;31m"),
    GREEN("\u001b[0;32m"),
    YELLOW("\u001b[0;33m"),
    BLUE("\u001b[0;34m"),
    PURPLE("\u001b[0;35m"),
    CYAN("\u001b[0;36m"),
    WHITE("\u001b[0;37m")
}

fun main() {
    val rectangle1 = RectangleFigure("${Figure.RED.value}X${Figure.RESET.value}", 5, 4)
    val rectangle2 = RectangleFigure("${Figure.YELLOW.value}X${Figure.RESET.value}", 2, 2)
    val rectangle3 = RectangleFigure("${Figure.GREEN.value}X${Figure.RESET.value}", 5, 3)

    rectangle1.paintColors()
    println()
    rectangle2.paintColors()
    println()
    rectangle3.paintColors()
}
