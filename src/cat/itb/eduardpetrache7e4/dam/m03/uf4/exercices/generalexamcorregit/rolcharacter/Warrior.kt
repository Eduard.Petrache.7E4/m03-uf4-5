package cat.itb.eduardpetrache7e4.dam.m03.uf4.exercices.generalexamcorregit.rolcharacter

class Warrior(name: String, healthPoints: Int) : Character(name, healthPoints) {
    override fun attack(opponent: IRolCharacter) {
        super.attack(opponent)
        println("$name is using a sword to attack ${opponent.name}")
    }
}