package cat.itb.eduardpetrache7e4.dam.m03.uf4.exercices.inter

import com.google.gson.Gson
import java.net.URL

class BcnTreeFinder : TreeFinder {
    override fun countTreesByScientificName(scientificName: String): Int {
        // Llegim les dades dels arbres
        val url = "https://opendata-ajuntament.barcelona.cat/resources/bcn/Arbrat/OD_Arbrat_Zona_BCN.json"
        val jsonString = URL(url).readText()

        // Convertim el JSON a una llista d'arbres
        val gson = Gson()
        val trees = gson.fromJson(jsonString, Array<Tree>::class.java).toList()

        // Calculem el nombre d'arbres amb el nom científic especificat
        return trees.count { it.nom_cientific.equals(scientificName, ignoreCase = true) }
    }
}