package cat.itb.eduardpetrache7e4.dam.m03.uf4.exercices.gymcontrol

class GymControlApp(private val reader: GymControlReader) {
    fun start() {
        val ids = mutableListOf<String>()
        repeat(8) {
            ids.add(reader.nextId())
        }
        val enteredIds = mutableSetOf<String>()
        for (id in ids) {
            if (enteredIds.contains(id)) {
                enteredIds.remove(id)
                println("$id Sortida")
            } else {
                enteredIds.add(id)
                println("$id Entrada")
            }
        }
    }
}

fun main() {
    val reader = GymControlManualReader()
    val app = GymControlApp(reader)
    app.start()
}

