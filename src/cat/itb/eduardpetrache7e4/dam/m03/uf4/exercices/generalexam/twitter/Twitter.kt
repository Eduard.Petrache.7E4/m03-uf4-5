package cat.itb.eduardpetrache7e4.dam.m03.uf4.exercices.generalexam.twitter

fun main() {
    val tweet1 = Tweet("iamdevloper", "07 de gener", "Remember, a few hours of trial and error can save you several minutes of looking at the README")
    val tweet2 = Tweet("softcatala", "29 de març", "Avui mateix, #CommonVoiceCAT segueix creixent \uD83D\uDE80:\n\uD83D\uDDE3️ 856 hores enregistrades\n✅ 725 de validades.\nSi encara no has participat, pots fer-ho aquí!")
    val polltweet3 = PollTweet("musicat", "02 d'abril", "Quina cançó t'agrada més?", "Comèdia dramàtica - La Fúmiga")
    val tweet4 = Tweet( "ProgrammerJokes", "05 d'abril", "Q: what's the object-oriented way to become weathy?\n\nA: Inheritance")
    print(tweet1)
    print(tweet2)
    print(polltweet3)
    print(tweet4)
}