package cat.itb.eduardpetrache7e4.dam.m03.uf4.exercices.shop

class ScooterModel(private val name: String, private val power: Double, private val brand: VehicleBrand) {
    override fun toString(): String {
        return "ScooterModel{name='$name', power=$power, brand=$brand}"
    }
}

fun main() {
    val brand = VehicleBrand("lorem", "Togo")
    val bicycle1 = BicycleModel("ModelX", 5, brand)
    val scooter1 = ScooterModel("s562", 45.3, brand)

    println(bicycle1)
    println(scooter1)
}