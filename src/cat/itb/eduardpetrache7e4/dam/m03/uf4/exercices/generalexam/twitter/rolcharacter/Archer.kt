package cat.itb.eduardpetrache7e4.dam.m03.uf4.exercices.generalexam.twitter.rolcharacter

class Archer (override var name: String, override var healthPoints: Int): Character("Archer", name, healthPoints) {
    override fun attack(opponent: Character) {
        println("$name is shooting arrows at Gandalf")
    }

    override fun takeDamage(damage: Int) {
        TODO("Not yet implemented")
    }
}
