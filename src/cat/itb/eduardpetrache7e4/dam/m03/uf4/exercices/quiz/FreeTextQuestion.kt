package cat.itb.eduardpetrache7e4.dam.m03.uf4.exercices.quiz

class FreeTextQuestion(text: String, val correctAnswer: String) : Question(text) {
    override fun checkAnswer(answer: String): Boolean {
        return answer.equals(correctAnswer, ignoreCase = true)
    }
}