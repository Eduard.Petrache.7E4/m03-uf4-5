package cat.itb.eduardpetrache7e4.dam.m03.uf4.exercices.motto

object MottoApp {
    @JvmStatic
    fun main(args: Array<String>) {
        val teams: List<Team> = listOf(Mosquitoes(), Dragons(), Bees())
        shoutMottos(teams)
    }

    private fun shoutMottos(teams: List<Team>) {
        for (team in teams) {
            team.shoutMotto()
        }
    }
}
