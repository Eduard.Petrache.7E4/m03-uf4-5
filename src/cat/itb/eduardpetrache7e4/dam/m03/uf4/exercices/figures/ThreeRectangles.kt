package cat.itb.eduardpetrache7e4.dam.m03.uf4.exercices.figures


class RectangleFigure(private val color: String, private val width: Int, private val height: Int) {
    fun paint() {
        for (i in 1..height) {
            for (j in 1..width) {
                print(color.substring(0, 1))
            }
            println()
        }
    }

    fun paintColors() {
        for (i in 1..height) {
            for (j in 1..width) {
                System.out.print(color)
            }
            System.out.println(RESET)
        }
    }
}


fun main() {
    val rectangle1 = RectangleFigure("RED", 5, 4)
    val rectangle2 = RectangleFigure("YELLOW", 2, 2)
    val rectangle3 = RectangleFigure("GREEN", 5, 3)

    rectangle1.paint()
    println()
    rectangle2.paint()
    println()
    rectangle3.paint()
}

