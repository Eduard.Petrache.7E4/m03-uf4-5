package cat.itb.eduardpetrache7e4.dam.m03.uf4.exercices.inter

interface TreeFinder {
    fun countTreesByScientificName(scientificName: String): Int
}