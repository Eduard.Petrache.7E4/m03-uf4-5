package cat.itb.eduardpetrache7e4.dam.m03.uf4.exercices.carpentryshop

interface Product {
    fun calculatePrice(): Double
}