package cat.itb.eduardpetrache7e4.dam.m03.uf4.exercices.generalexamcorregit.rolcharacter

abstract class Character(override var name: String, override var healthPoints: Int) : IRolCharacter {
    override fun attack(opponent: IRolCharacter) {
        println("$name is attacking ${opponent.name}")
        opponent.takeDamage(10)
    }

    override fun takeDamage(damage: Int) {
        healthPoints -= damage
        if (healthPoints <= 0) {
            println("$name has been defeated!")
        } else {
            println("$name took $damage points of damage and has $healthPoints health points left.")
        }
    }
}