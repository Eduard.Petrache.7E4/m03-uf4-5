package cat.itb.eduardpetrache7e4.dam.m03.uf4.exercices.shop

class BicycleModel(private val name: String, private val gears: Int, private val brand: VehicleBrand){
    override fun toString(): String {
        return "BicycleModel{name='$name', gears=$gears, brand=$brand}"
    }
}
class VehicleBrand(private val name: String, private val country: String){
    override fun toString(): String {
        return "VehicleBrand{name='$name', country='$country'}"
    }
}

fun main() {
    val brand = VehicleBrand("Specialized", "USA")
    val bicycle1 = BicycleModel("Jett 24", 5, brand)
    val bicycle2 = BicycleModel("Hotwalk", 7, brand)

    println(bicycle1)
    println(bicycle2)
}

