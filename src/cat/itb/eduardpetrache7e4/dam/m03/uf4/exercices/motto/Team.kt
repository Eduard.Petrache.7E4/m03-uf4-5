package cat.itb.eduardpetrache7e4.dam.m03.uf4.exercices.motto

interface Team {
    val name: String
    val motto: String
    fun shoutMotto()
}
