package cat.itb.eduardpetrache7e4.dam.m03.uf4.exercices.figures

const val RESET = "\u001b[0m" // Text Reset
const val BLACK = "\u001b[0;30m" // BLACK
const val RED = "\u001b[0;31m" // RED
const val GREEN = "\u001b[0;32m" // GREEN
const val YELLOW = "\u001b[0;33m" // YELLOW
const val BLUE = "\u001b[0;34m" // BLUE
const val PURPLE = "\u001b[0;35m" // PURPLE
const val CYAN = "\u001b[0;36m" // CYAN
const val WHITE = "\u001b[0;37m" // WHITE

fun main(){
    val rectangle1 = RectangleFigure("${RED}X${RESET}", 5, 4)
    val rectangle2 = RectangleFigure("${YELLOW}X${RESET}", 2, 2)
    val rectangle3 = RectangleFigure("${GREEN}X${RESET}", 5, 3)

    rectangle1.paint()
    println()
    rectangle2.paint()
    println()
    rectangle3.paint()
}