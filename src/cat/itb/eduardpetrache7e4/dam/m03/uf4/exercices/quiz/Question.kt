package cat.itb.eduardpetrache7e4.dam.m03.uf4.exercices.quiz

abstract class Question(val text: String) {
    abstract fun checkAnswer(answer: String): Boolean
}