package cat.itb.eduardpetrache7e4.dam.m03.uf4.exercices.instrumentsimulator

abstract class Instrument {
    abstract fun makeSound(): String
    fun makeSounds(n: Int) {
        repeat(n) {
            println(makeSound())
        }
    }
}