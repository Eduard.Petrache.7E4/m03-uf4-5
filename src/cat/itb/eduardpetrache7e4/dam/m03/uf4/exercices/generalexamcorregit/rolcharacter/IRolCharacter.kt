package cat.itb.eduardpetrache7e4.dam.m03.uf4.exercices.generalexamcorregit.rolcharacter

interface IRolCharacter {
    var name: String
    var healthPoints: Int
    fun attack(opponent: IRolCharacter)
    fun takeDamage(damage: Int)
}