package cat.itb.eduardpetrache7e4.dam.m03.uf4.exercices.motto

class Dragons : Team {
    override val name = "Dragons"
    override val motto = "Grooarg"

    override fun shoutMotto() {
        println(motto)
    }
}
