package cat.itb.eduardpetrache7e4.dam.m03.uf4.exercices.generalexam.twitter.rolcharacter

class Wizard (override var name: String, override var healthPoints: Int): Character("Wizard", name, healthPoints){
    override fun attack(opponent: Character) {
        println("$name is casting a spell on Thor")
    }

    override fun takeDamage(damage: Int) {
        TODO("Not yet implemented")
    }
}
