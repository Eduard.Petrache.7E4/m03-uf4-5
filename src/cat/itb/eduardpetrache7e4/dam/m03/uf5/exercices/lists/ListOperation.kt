package cat.itb.eduardpetrache7e4.dam.m03.uf5.exercices.lists

fun main() {
    val input = readLine()!!.split(" ").map { it.toInt() }
    val numbers = input.subList(1, input.size)
        .filter { it % 10 != 3 }
        .sortedDescending()
    numbers.forEach { println(it) }
}
