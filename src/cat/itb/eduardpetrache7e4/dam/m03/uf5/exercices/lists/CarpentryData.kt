package cat.itb.eduardpetrache7e4.dam.m03.uf5.exercices.lists

import cat.itb.eduardpetrache7e4.dam.m03.uf4.exercices.carpentryshop.Llisto
import cat.itb.eduardpetrache7e4.dam.m03.uf4.exercices.carpentryshop.Product
import cat.itb.eduardpetrache7e4.dam.m03.uf4.exercices.carpentryshop.Taulell

fun main() {
    val productList = mutableListOf<Product>()

    val n = readLine()!!.toInt()
    repeat(n) {
        val productLine = readLine()!!.split(" ")
        val type = productLine[0]
        val params = productLine.subList(1, productLine.size).map { it.toDouble() }

        val product = when (type) {
            "Taulell" -> Taulell(params[0], params[1], params[2])
            "Llistó" -> Llisto(params[0], params[1])
            else -> throw IllegalArgumentException("Tipus de producte desconegut: $type")
        }

        productList.add(product)
    }

    // Preu total dels llistons venuts
    val llistos = productList.filterIsInstance<Llisto>()
    val totalLlistosPrice = llistos.sumOf { it.calculatePrice() }
    println("Preu total llistons: $totalLlistosPrice")

    // Llargada del producte més car
    val productMaxPrice = productList.maxByOrNull { it.calculatePrice() }!!
    val maxLength = when (productMaxPrice) {
        is Taulell -> productMaxPrice.length
        is Llisto -> productMaxPrice.length
        else -> throw IllegalArgumentException("Tipus de producte desconegut: ${productMaxPrice::class.simpleName}")
    }
    println("Producte més car té una llargada de: $maxLength")

    // Quantitat de productes de més de 100€
    val expensiveProducts = productList.filter { it.calculatePrice() > 100 }
    val numExpensiveProducts = expensiveProducts.size
    println("Num productés de més de 100€: $numExpensiveProducts")
}
